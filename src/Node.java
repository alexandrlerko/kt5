
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      name = n;
      firstChild = d;
      nextSibling = r;
   }

   public static Node parsePostfix (String s) {
       Pattern pattern = Pattern.compile("[)\\w(]");
       Matcher matcher = pattern.matcher(s);
       String[] splitted = s.split(",");
      if (s.contains("\t")
              || s.length() == 0
              || s.contains(" ")
//              || matcher.lookingAt()
              || (s.contains("((") && s.contains("))"))
              || (!s.contains("(") && !s.contains(")")) && s.contains(",")){
         throw new RuntimeException("Etteantav sõne on vigane:" + s);
      }
      int countRight = 0;
      int countLeft = 0;
      StringBuilder firstChild = new StringBuilder();
      StringBuilder nextSibling = new StringBuilder();
      if (!s.contains("(") && !s.contains(")")){
         firstChild.append(s);
         Node j = new Node(firstChild.toString(), null, null);
         return new Node(s, j, null);
      }
        String[] stringArray = s.split("");
      int index = 0;
      for (String element : stringArray) {
         index++;
         if (element.equals("(") && countLeft == 0) {
            countLeft++;
            continue;
         } else if (element.equals("(") && countLeft > 0){
            countLeft++;
            nextSibling.append(element);
            continue;
         } else if (element.equals(")") && index == s.length() - 1){
            countRight++;
            continue;
         } else if (element.equals(")")) {
            countRight++;
            nextSibling.append(element);
            continue;
         }
         if (countLeft == countRight && stringArray[s.length() - 1].equals(element)) {
            firstChild.append(element);
         } else {
            nextSibling.append(element);
         }
      }
      Node firstChildNode = new Node(firstChild.toString(), null, null);
      Node nextSiblingNode = new Node(nextSibling.toString(), null, null);
      if (nextSibling.toString().length() == 0){
         nextSiblingNode = new Node(null, null, null);
      }
      Node ready = new Node(s, firstChildNode, nextSiblingNode);
      String[] nextSiblingSplited = nextSibling.toString().split("");
      int extraLeft = 0;
      int extraRight = 0;
      int word = 0;
       for (String extraElement : nextSiblingSplited) {
           if (extraElement.equals("(")){
               extraRight++;
           } else if (extraElement.equals(")")){
               extraLeft++;
           } else {
               word++;
           }
           if ((word == 1 && extraLeft != 0 && extraRight == 0)
                   || (word == 1 && extraLeft == 0 && extraRight != 0) && extraElement.equals("(")) {
               throw new RuntimeException("Etteantav sõne on vigane: " + s);
           }

       }
      if (ready.nextSibling.name == null
              || ready.nextSibling.name.contains(",,")
              || ready.nextSibling.name.matches(",\\w")
              || firstChild.toString().length() == 0
              || nextSiblingSplited[nextSiblingSplited.length - 1].equals(",")){
          throw new RuntimeException("Etteantav sõne on vigane: " + s);
      }
      return ready;
   }

    public String leftParentheticRepresentation() {
        Node obj = this;
        if (obj.nextSibling == null && obj.firstChild == null) {
            return obj.name;
        } else if (obj.nextSibling == null){
            return obj.firstChild.name;
        }
        StringBuffer answer = new StringBuffer(); // final answer have to be here
        answer.append(obj.firstChild.name + "("); // adding root
        String i = obj.nextSibling.name; // getting nextSibling into a String value
        String toDelete = "";
        StringBuffer saver = goDepth(i);
        StringBuffer test = new StringBuffer();
        while (i.length() != 0){
            if (!i.equals(obj.nextSibling.name)){
                String[] checker = i.split("");
                if (checker[0].equals(",")){
                    i = i.substring(1);
                    answer.append(",");
                }
                saver = goDepth(i);
                test = new StringBuffer();
                obj.nextSibling.name = i;
            }
            String saverValidCheck = saver.toString();
            if (test.length() == 0){
                toDelete = saver.toString();
            }
            if (saverValidCheck.contains("(") && saverValidCheck.contains(")")){
                Node myObj = parsePostfix(saver.toString());
                if (!myObj.name.equals(myObj.firstChild.name)){
                    answer.append(myObj.firstChild.name);
                    String[] splitLoop1 = myObj.name.split("");
                    if (test.length() == 0){
                        for (String value : splitLoop1) {
                            if (!value.equals(myObj.firstChild.name)) {
                                test.append(value);
                            }
                        }
                    } else {
                        String[] splitLoop2 = test.toString().split("");
                        StringBuffer local = new StringBuffer();
                        for (String value2 : splitLoop2) {
                            if (!value2.equals(myObj.firstChild.name)) {
                                local.append(value2);
                            }
                        }
                        test = local;
                    }
                    String[] splitTest = test.toString().split("");
                    answer.append(splitTest[0]);
                    test.delete(0, 1);
                    if (saver.length() != 0) {
                        String[] elementsCheck = myObj.nextSibling.name.split("");
                        if (!elementsCheck[0].equals("(") && !elementsCheck[elementsCheck.length - 1].equals(")")
                                && elementsCheck.length > 1) {
                            StringBuffer checker = new StringBuffer();
                            for (String elementValue : elementsCheck) {
                                if (elementValue.equals(",")) {
                                    break;
                                }
                                checker.append(elementValue);
                            }
                            String[] commaChecker = answer.toString().split("");
                            if (commaChecker[commaChecker.length - 1].equals(",")){
                                answer.append(checker.toString());
                                test.delete(0, 2);
                            } else {
                                answer.append(checker.toString() + ",");
                                test.delete(0, checker.length() + 1);
                            }
                        }
                        saver = (goDepth(myObj.nextSibling.name));
                    }
                } else{
                    if (myObj.firstChild.name.equals(obj.nextSibling.name)){
                        answer.append(myObj.firstChild.name);
                    }
                    String[] splitCommaCheck = answer.toString().split("");
                    String[] splitCommaCheck2 = test.toString().split("");
                    if (splitCommaCheck[splitCommaCheck.length - 1].equals(splitCommaCheck2[0])){
                        answer.append(test.substring(1, 2));
                        i = i.substring(toDelete.length());
                    } else {
                        answer.append(test);
                        i = i.substring(toDelete.length());
                    }
                }
            } else {
                if (test.length() == 0) {
                    answer.append(saver);
                }
                String[] commaCheck = answer.toString().split("");
                String[] commaCheck2 = test.toString().split("");
                if (commaCheck[commaCheck.length - 1].equals(commaCheck2[0])){
                    answer.append(test.substring(1, 2));
                    i = i.substring(toDelete.length());
                } else {
                    answer.append(test);
                    i = i.substring(toDelete.length());
                }
            }
        }
        answer.append(")");
        return answer.toString();
    }

    public String pseudoXml(){
       Node obj = this;
       StringBuilder answer = new StringBuilder();
       String leftRep = leftParentheticRepresentation();
       if (!leftRep.contains("(") && !leftRep.contains(")")) {
           return "<L1> " + obj.name + " </L1>";
       }
       int countLevel = 1;
       String[] splited = leftRep.split("");
       int index1 = 0;
        for (String element : splited) {
            if (element.equals("(")) {
                countLevel++;
            } else if (element.equals(")")) {
                countLevel--;
                answer.append(tabs(countLevel)).append(" </L").append(countLevel).append(">").append("\n");
            }
            if (!element.equals("(") && !element.equals(")") && !element.equals(",") && !element.equals("")) {
                String i = "";
                int index2 = 0;
                for (int j = index1; j < splited.length; j++) {
                    if (!splited[j].equals("(") && !splited[j].equals(")") && !splited[j].equals(",") && !splited[j].equals("")) {
                        i += splited[j];
                        splited[j] = "";
                        index2++;
                    } else {
                        break;
                    }
                }
                if (splited[index1 + index2].equals("(")) {
                    answer.append(tabs(countLevel)).append("<L").append(countLevel).append("> ").append(i).append("\n");
                } else {
                    answer.append(tabs(countLevel)).append("<L").append(countLevel).append("> ").append(i).append(" </L").append(countLevel).append(">").append("\n");
                }
            }
            index1++;
        }
       return answer.toString();
    }

    public String tabs(int value) {
       StringBuilder tabs = new StringBuilder();
        for (int j = 0; j < value; j++) {
            tabs.append("\t");
        }
        return tabs.toString();
    }

   public StringBuffer goDepth(String string) {
       if (!string.contains("(") && !string.contains(")")){
           return new StringBuffer(string);
       } else if (!string.contains("(")  && string.contains(")")) {
           return new StringBuffer(string);
       } else if (string.contains("(")  && !string.contains(")")) {
           return new StringBuffer(string);
       }
       String[] split = string.split("");
       StringBuffer saver = new StringBuffer();
       int right = 0;
       int left = 0;
       int index = 0;
       int indexStart = 0;
       int indexEnd = 0;
       for (String value : split) {
           if (value.equals(")")) {
               left++;
           } else if (value.equals("(") && right == 0) {
               indexStart = index;
               right++;
           } else if (value.equals("(")) {
               right++;
           }
           if (right == left && index != 0 && right != 0) {
               indexEnd = index;
               saver.append(string, indexStart, indexEnd + 2);
           }
           index++;
           if (saver.length() != 0){
               return saver;
           }
       }
       return saver;
   }

   public static void main (String[] param) {

      String s = "(B1,C)A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      String w = t.pseudoXml();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
      System.out.println (w); // (B1,C)A ==> A(B1,C)

       String cc = "(((512,1)-,4)*,(-6,3)/)+";
       Node gg = Node.parsePostfix (cc);
       String bb = gg.leftParentheticRepresentation();
       System.out.println(cc + " ==> " + bb); // (((512,1)-,4)*,(-6,3)/)+ ==> +(*(-(512,1),4),/(-6,3))

      String ss = "(((2,1)-,4)*,(69,3)/)+";
      Node tt = Node.parsePostfix (ss);
      String vv = tt.leftParentheticRepresentation();
      System.out.println(ss + " ==> " + vv); // (((2,1)-,4)*,(69,3)/)+ ==> +(*(-(2,1),4),/(69,3))

       String sss = "((1,(2)3,4)5)6";
       Node ttt = Node.parsePostfix (sss);
       String vvv = ttt.leftParentheticRepresentation();
       System.out.println(sss + " ==> " + vvv); // ((1,(2)3,4)5)6 ==> 6(5(1,3(2),4))

       String ssss = "((C,(E)D)B,F)A";
       Node tttt = Node.parsePostfix (ssss);
       String vvvv = tttt.leftParentheticRepresentation();
       System.out.println(ssss + " ==> " + vvvv); // ((C,(E)D)B,F)A ==> A(B(C,D(E)),F)
   }
}

